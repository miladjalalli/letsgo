package webservices;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by boolean on 12/13/2017.
 */

public class WebServices {

    public static final String STR_URL_LOC = "http://192.168.82.201/get_location.php";
    public static final String STR_URL_USER_DETAIL = "http://192.168.82.201/get_user_detail.php";
    public static final String STR_URL_COIN_CHANGE = "http://192.168.82.201/coin_update.php";
    public static final String STR_URL_ADS = "http://192.168.82.201/places.php";
    public static final String STR_URL_USER_INFO = "http://192.168.82.201/get_user_info.php";
    public static final String STR_URL_PROFILE_INFO = "http://192.168.82.201/profile_info.php";
    public static final String STR_URL_LOGIN = "http://192.168.82.201/login.php";
    public static final String STR_URL_REGISTER = "http://192.168.82.201/register.php";
    public static final String STR_URL_VERIFY = "http://192.168.82.201/verify.php";

    private Context context;
    private String strPassword;

    WebResponse webResponseRegister = new WebResponse();

    public WebServices(Context context) {
        this.context = context;
    }






    public void storeChat(final String strSender, final String strReceiver, final String strMessage) {
        Response.Listener<String> locResponse = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);

                    String code = jsonObject.getString("code");
                    String message = jsonObject.getString("message");

                    if(code.equals("login_failed")) {
                        Toast.makeText(context, "Login failed : " + message , Toast.LENGTH_LONG).show();
                    } else if(code.equals("login_success")) {
                        Toast.makeText(context, "Login done : " + message , Toast.LENGTH_LONG).show();
                        // verify account
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener locErrorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.POST, STR_URL_LOGIN, locResponse, locErrorListener) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("sender", String.valueOf(strSender));
                params.put("receiver", String.valueOf(strReceiver));
                params.put("message", String.valueOf(strMessage));

                return params;
            }
        };

        MinaSingleton.getInstance(context).addToRequestQueue(stringRequest);
    }
}

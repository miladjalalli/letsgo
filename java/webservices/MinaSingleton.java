package webservices;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by boolean on 12/11/2017.
 */

public class MinaSingleton {
    private static MinaSingleton minaSingleton;
    private RequestQueue requestQueue;
    private static Context context;

    private MinaSingleton(Context context) {
        this.context = context;
        requestQueue = getRequestQueue();
    }

    public RequestQueue getRequestQueue() {
        if(requestQueue == null) {
            requestQueue = Volley.newRequestQueue(context.getApplicationContext());
        }

        return requestQueue;
    }

    public static synchronized MinaSingleton getInstance(Context context) {
        if (minaSingleton == null) {
            minaSingleton = new MinaSingleton(context);
        }

        return minaSingleton;
    }

    public <T>void addToRequestQueue(Request<T> request) {
        requestQueue.add(request);
    }
}

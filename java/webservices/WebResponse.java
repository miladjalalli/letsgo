package webservices;

/**
 * Created by Milad on 12/13/2017.
 */

public class WebResponse {

    public String strCode;
    public String strMessage;

    public WebResponse() {}

    public WebResponse(String strCode, String strMessage) {
        this.strCode = strCode;
        this.strMessage = strMessage;
    }
}

/*new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Log.d("link", carsArrayAdapter.get(position).getLogo());
                    URL newurl = new URL(carsArrayAdapter.get(position).getLogo());
                    final Bitmap mIcon_val = BitmapFactory.decodeStream(newurl.openConnection().getInputStream());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            imgAvatar.setImageBitmap(mIcon_val);
                        }
                    });

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();*/

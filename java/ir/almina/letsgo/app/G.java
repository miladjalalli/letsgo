package ir.almina.letsgo.app;

import android.app.Application;

/**
 * Created by Ravi on 13/05/15.
 */

public class G extends Application {

    public static int height;
    public static int width;

    public static final String TAG = G.class.getSimpleName();
    private static G mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

    }

    public static synchronized G getInstance() {
        return mInstance;
    }

}
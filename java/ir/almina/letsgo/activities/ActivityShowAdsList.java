package ir.almina.letsgo.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ListView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import ir.almina.letsgo.Models.Ads;
import ir.almina.letsgo.Models.Cars;
import ir.almina.letsgo.R;
import ir.almina.letsgo.UiFont;
import ir.almina.letsgo.adapters.adsAdapter;
import ir.almina.letsgo.adapters.carsAdapter;
import webservices.MinaSingleton;

import static webservices.WebServices.STR_URL_ADS;
import static webservices.WebServices.STR_URL_LOC;

public class ActivityShowAdsList extends AppCompatActivity {


    ArrayList<Ads> adsArrayList = new ArrayList<>();
    ListView listViewAds;
    ProgressDialog progressDialog;

    @Override
    protected void onResume() {
        super.onResume();
        adsArrayList.clear();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                progressDialog = new ProgressDialog(ActivityShowAdsList.this);
                progressDialog.setTitle("در حال بارگذاری...");
                progressDialog.show();
            }
        }, 100);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getMatchPlaces();
            }
        }, 100);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_ads_list);
        UiFont.persianizer((ViewGroup) getWindow().getDecorView(), getApplicationContext());
        InitViews();

    }

    private void InitViews(){
        listViewAds = (ListView) findViewById(R.id.listViewAds);
    }

    public void getMatchPlaces(/*final double dblSourceLat, final double dblSourceLong, final String strSourceAddress,
                              final double dblDestinationLat, final double dblDestinationLong, final String strDestinationAddress,
                              final boolean blnOnlyOAE*/) {

        Response.Listener<String> locResponse = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String strName = jsonObject.getString("name");
                        String strAddress = jsonObject.getString("address");
                        String strPhone = jsonObject.getString("phone");
                        String strAvatar = jsonObject.getString("avatar_link");

                        Ads ads = new Ads(strName, strAddress, strPhone,strAvatar);
                        adsArrayList.add(ads);
                    }
                    adsAdapter adsAdapter = new adsAdapter(ActivityShowAdsList.this, adsArrayList);
                    listViewAds.setAdapter(adsAdapter);
                    progressDialog.dismiss();


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener locErrorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.POST, STR_URL_ADS, locResponse, locErrorListener) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                return params;
            }
        };

        MinaSingleton.getInstance(ActivityShowAdsList.this).addToRequestQueue(stringRequest);
    }
    boolean backPressed = false;
    @Override
    public void onBackPressed() {
        if(!backPressed){
            final Dialog dialog = new Dialog(ActivityShowAdsList.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_end_ad);
            dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            dialog.show();

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    dialog.dismiss();
                    finish();
                }
            },2500);
            backPressed = true;
        }

    }


}

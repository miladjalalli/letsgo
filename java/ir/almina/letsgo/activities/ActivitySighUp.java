package ir.almina.letsgo.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import ir.almina.letsgo.R;
import ir.almina.letsgo.SharedPreferencesHelper;
import ir.almina.letsgo.UiFont;
import ir.almina.letsgo.app.G;
import webservices.MinaSingleton;
import webservices.WebResponse;
import webservices.WebServices;

import static webservices.WebServices.STR_URL_LOGIN;
import static webservices.WebServices.STR_URL_REGISTER;
import static webservices.WebServices.STR_URL_VERIFY;

public class ActivitySighUp extends AppCompatActivity {

    SharedPreferencesHelper sharedPreferencesHelper;
    ImageView imgLogo;
    Button isUserExist, isGuest, btnRequest, btnVarify;
    LinearLayout llLisencePlate;
    TextView txtLisencePlate, space;
    TextInputLayout tilFirstName, tilLastName, tilMobileNumber, tilNationalCode, tilSmsCode;
    EditText edtFirstName, edtLastName, edtMobileNumber, edtNationalCode, edtLisencePlate12, edtLisencePlate3,
            edtLisencePlate46, edtLisencePlate78, edtSmsCode;
    boolean isNew = true;
    private WebServices webServices;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferencesHelper = new SharedPreferencesHelper(ActivitySighUp.this);
        setContentView(R.layout.activity_sigh_up);
        webServices = new WebServices(this);
        UiFont.persianizer((ViewGroup) getWindow().getDecorView(), getApplicationContext());
        InitViews();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        tilFirstName.requestFocus();

        isUserExist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isNew = false;
                tilFirstName.setVisibility(View.GONE);
                tilLastName.setVisibility(View.GONE);
                tilNationalCode.setVisibility(View.GONE);
                llLisencePlate.setVisibility(View.GONE);
                space.setVisibility(View.GONE);
                isUserExist.setVisibility(View.GONE);
                txtLisencePlate.setVisibility(View.GONE);
                isGuest.setVisibility(View.VISIBLE);
            }
        });
        isGuest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isNew = true;
                tilFirstName.setVisibility(View.VISIBLE);
                tilLastName.setVisibility(View.VISIBLE);
                tilNationalCode.setVisibility(View.VISIBLE);
                llLisencePlate.setVisibility(View.VISIBLE);
                isUserExist.setVisibility(View.VISIBLE);
                space.setVisibility(View.VISIBLE);
                txtLisencePlate.setVisibility(View.VISIBLE);
                isGuest.setVisibility(View.GONE);

            }
        });
        btnRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isNew) {
                    int error = 0;
                    if (edtFirstName.getText().length() < 3) {
                        edtFirstName.setError("نام وارد شده خیلی کوتاه است");
                        error++;
                    }
                    if (edtLastName.getText().length() < 3) {
                        edtLastName.setError("نام خانوادگی وارد شده خیلی کوتاه است");
                        error++;
                    }
                    if (edtMobileNumber.getText().length() != 11) {
                        edtMobileNumber.setError("شماره موبایل وارد شده صحیح نیست");
                        error++;
                    }
                    if (edtNationalCode.getText().length() != 10) {
                        edtNationalCode.setError("شماره ملی وارد شده صحیح نیست");
                        error++;
                    }
                    if (edtLisencePlate12.getText().length() < 2) {
                        edtLisencePlate12.setError("مقدار وارد شده صحیح نیست");
                        error++;
                    }
                    if (edtLisencePlate3.getText().length() != 1) {
                        edtLisencePlate3.setError("مقدار وارد شده صحیح نیست");
                        error++;
                    }
                    if (edtLisencePlate46.getText().length() < 3) {
                        edtLisencePlate46.setError("مقدار وارد شده صحیح نیست");
                        error++;
                    }
                    if (edtLisencePlate78.getText().length() < 2) {
                        edtLisencePlate78.setError("مقدار وارد شده صحیح نیست");
                        error++;
                    }
                    if (error == 0) {

                        register(edtFirstName.getText().toString(),
                                edtLastName.getText().toString(),
                                edtLisencePlate12.getText().toString() + edtLisencePlate3.getText() + edtLisencePlate46.getText() + edtLisencePlate78.getText(),
                                edtNationalCode.getText().toString(),
                                "Pride",
                                edtMobileNumber.getText().toString(),
                                (Integer.valueOf(edtLisencePlate46.getText().toString()) % 2 == 0 ? "even" : "odd"),
                                "male"
                        );

                    }
                } else {
                    int error = 0;
                    if (edtMobileNumber.getText().length() != 11) {
                        edtMobileNumber.setError("شماره موبایل وارد شده صحیح نیست");
                        error++;
                    }
                    if (error == 0) {
                        login(edtMobileNumber.getText().toString());
                        tilFirstName.setVisibility(View.GONE);
                        tilLastName.setVisibility(View.GONE);
                        tilNationalCode.setVisibility(View.GONE);
                        llLisencePlate.setVisibility(View.GONE);
                        space.setVisibility(View.GONE);
                        isUserExist.setVisibility(View.GONE);
                        txtLisencePlate.setVisibility(View.GONE);
                        isGuest.setVisibility(View.GONE);
                        btnRequest.setVisibility(View.GONE);
                        tilMobileNumber.setVisibility(View.GONE);
                        btnVarify.setVisibility(View.VISIBLE);
                        tilSmsCode.setVisibility(View.VISIBLE);
                    }
                }

            }
        });

        btnVarify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifyAccount(edtMobileNumber.getText().toString(), edtSmsCode.getText().toString());
            }
        });

        btnRequest.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                sharedPreferencesHelper.setBooleanKey(sharedPreferencesHelper.Key_IsLogin, true);
                startActivity(new Intent(ActivitySighUp.this, ActivityMain.class));
                ActivitySighUp.this.finish();
                return false;
            }
        });
    }

    private void InitViews() {
        imgLogo = (ImageView) findViewById(R.id.imgLogo);


        isUserExist = (Button) findViewById(R.id.isUserExist);
        isGuest = (Button) findViewById(R.id.isGuest);
        btnRequest = (Button) findViewById(R.id.btnRequest);
        btnVarify = (Button) findViewById(R.id.btnVarify);
        tilFirstName = (TextInputLayout) findViewById(R.id.tilFirstName);
        tilLastName = (TextInputLayout) findViewById(R.id.tilLastName);
        tilNationalCode = (TextInputLayout) findViewById(R.id.tilNationalCode);
        tilMobileNumber = (TextInputLayout) findViewById(R.id.tilMobileNumber);
        tilSmsCode = (TextInputLayout) findViewById(R.id.tilSmsCode);
        edtFirstName = (EditText) findViewById(R.id.edtFirstName);
        edtLastName = (EditText) findViewById(R.id.edtLastName);
        edtNationalCode = (EditText) findViewById(R.id.edtNationalCode);
        edtMobileNumber = (EditText) findViewById(R.id.edtMobileNumber);
        edtLisencePlate12 = (EditText) findViewById(R.id.edtLisencePlate12);
        edtLisencePlate3 = (EditText) findViewById(R.id.edtLisencePlate3);
        edtLisencePlate46 = (EditText) findViewById(R.id.edtLisencePlate46);
        edtLisencePlate78 = (EditText) findViewById(R.id.edtLisencePlate78);
        edtSmsCode = (EditText) findViewById(R.id.edtSmsCode);
        llLisencePlate = (LinearLayout) findViewById(R.id.llLisencePlate);
        txtLisencePlate = (TextView) findViewById(R.id.txtLisencePlate);
        space = (TextView) findViewById(R.id.space);
    }

    private void customToast(String text) {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.toast_layout_root));

        TextView textview = (TextView) layout.findViewById(R.id.text);
        textview.setText(text);

        Toast toast = new Toast(getApplicationContext());
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    public void register(final String strFirstName, final String strLastName, final String strLicensePlate, final String strPersonalCode,
                         final String strCarModel, final String strPhoneNumber, final String strLicenseType, final String strGender) {


        StringRequest stringRequest = new StringRequest(Request.Method.POST, STR_URL_REGISTER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            JSONObject jsonObject = jsonArray.getJSONObject(0);

                            String code = jsonObject.getString("code");
                            String message = jsonObject.getString("message");


                            if (code.equals("reg_success")) {
                                tilFirstName.setVisibility(View.GONE);
                                tilLastName.setVisibility(View.GONE);
                                tilNationalCode.setVisibility(View.GONE);
                                llLisencePlate.setVisibility(View.GONE);
                                space.setVisibility(View.GONE);
                                isUserExist.setVisibility(View.GONE);
                                txtLisencePlate.setVisibility(View.GONE);
                                isGuest.setVisibility(View.GONE);
                                btnRequest.setVisibility(View.GONE);
                                tilMobileNumber.setVisibility(View.GONE);
                                btnVarify.setVisibility(View.VISIBLE);
                                tilSmsCode.setVisibility(View.VISIBLE);
                            } else if (code.equals("reg_failed")) {
                                customToast("ثبت نام با شکست مواجه شد لطفا دوباره امتحان نمایید.");
                            } else if (code.equals("reg_found")) {
                                customToast("شما قبلا ثبت نام کرده اید!");
                                isNew = false;
                                tilFirstName.setVisibility(View.GONE);
                                tilLastName.setVisibility(View.GONE);
                                tilNationalCode.setVisibility(View.GONE);
                                llLisencePlate.setVisibility(View.GONE);
                                space.setVisibility(View.GONE);
                                isUserExist.setVisibility(View.GONE);
                                txtLisencePlate.setVisibility(View.GONE);
                                isGuest.setVisibility(View.VISIBLE);
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Error", error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("first_name", strFirstName);
                params.put("last_name", strLastName);
                params.put("license_plate", strLicensePlate);
                params.put("car_model", strCarModel);
                params.put("phone_number", strPhoneNumber);
                params.put("license_type", strLicenseType);
                params.put("gender", strGender);
                params.put("personal_code", strPersonalCode);

                return params;
            }
        };

        MinaSingleton.getInstance(this).addToRequestQueue(stringRequest);
    }

    public void verifyAccount(final String strPhoneNumber, final String strUserPassword) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, STR_URL_VERIFY,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            JSONObject jsonObject = jsonArray.getJSONObject(0);

                            String code = jsonObject.getString("code");
                            String message = jsonObject.getString("message");

                            if (code.equals("verify_failed")) {
                            } else if (code.equals("verify_done")) {
                                sharedPreferencesHelper.setBooleanKey(sharedPreferencesHelper.Key_IsLogin, true);
                                sharedPreferencesHelper.setStringKey(sharedPreferencesHelper.Key_Mobile, edtMobileNumber.getText().toString());
                                sharedPreferencesHelper.setStringKey(sharedPreferencesHelper.Key_Name, edtFirstName.getText().toString());
                                sharedPreferencesHelper.setStringKey(sharedPreferencesHelper.Key_Family, edtLastName.getText().toString());
                                String lisencePlate = edtLisencePlate12.getText().toString() + edtLisencePlate3.getText().toString() + edtLisencePlate46.getText().toString() + edtLisencePlate78.getText().toString();
                                sharedPreferencesHelper.setStringKey(sharedPreferencesHelper.Key_LisencePlate, lisencePlate);
                                sharedPreferencesHelper.setStringKey(sharedPreferencesHelper.Key_NationalCode, edtNationalCode.getText().toString());
                                startActivity(new Intent(ActivitySighUp.this, ActivityMain.class));
                                finish();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("phone_number", strPhoneNumber);
                params.put("user_password", strUserPassword);

                return params;
            }
        };

        MinaSingleton.getInstance(this).addToRequestQueue(stringRequest);
    }

    public void login(final String strPhoneNumber) {
        Response.Listener<String> locResponse = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);

                    String code = jsonObject.getString("code");
                    String message = jsonObject.getString("message");

                    if (code.equals("login_failed")) {
                        customToast("ورود با شکست مواجه شد لطفا دوباره امتحان نمایید.");
                    } else if (code.equals("login_success")) {
                        customToast("با موفقیت وارد شدید");
                        if (code.equals("reg_success")) {

                            btnRequest.setVisibility(View.GONE);
                            tilMobileNumber.setVisibility(View.GONE);
                            btnVarify.setVisibility(View.VISIBLE);
                            tilSmsCode.setVisibility(View.VISIBLE);
                        } else if (code.equals("reg_failed")) {
                            customToast("ثبت نام با شکست مواجه شد لطفا دوباره امتحان نمایید.");
                        } else if (code.equals("reg_found")) {
                            customToast("شما قبلا ثبت نام کرده اید!");
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener locErrorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.POST, STR_URL_LOGIN, locResponse, locErrorListener) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("phone_number", String.valueOf(strPhoneNumber));

                return params;
            }
        };

        MinaSingleton.getInstance(ActivitySighUp.this).addToRequestQueue(stringRequest);
    }
}

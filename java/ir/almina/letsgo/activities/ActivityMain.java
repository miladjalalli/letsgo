package ir.almina.letsgo.activities;

import android.Manifest;
import android.animation.Animator;
import android.app.ActivityOptions;
import android.app.Dialog;
import android.app.TabActivity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TabHost;
import android.widget.TextView;

import ir.almina.letsgo.R;
import ir.almina.letsgo.SharedPreferencesHelper;
import ir.almina.letsgo.UiFont;
import ir.almina.letsgo.app.G;
import tourguide.tourguide.Overlay;
import tourguide.tourguide.Pointer;
import tourguide.tourguide.ToolTip;
import tourguide.tourguide.TourGuide;


public class ActivityMain extends TabActivity {

    private RelativeLayout layout_tab;
    protected static ImageButton imgbtn_search_bg, imgbtn_timer_bg;
    public static TabHost tabHost;
    SharedPreferencesHelper sharedPreferencesHelper;
    private TourGuide mTourGuideHandler1, mTourGuideHandler2, mTourGuideHandler3, mTourGuideHandler4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferencesHelper = new SharedPreferencesHelper(ActivityMain.this);
        setContentView(R.layout.activity_main);
        UiFont.persianizer((ViewGroup) getWindow().getDecorView(), getApplicationContext());
        InitViews();
        setTabs();
        tabHost.setCurrentTab(0);

        ActivityCompat.requestPermissions(ActivityMain.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_NETWORK_STATE,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION}, 1);

        imgbtn_search_bg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent FullScannerActivityIntent = new Intent(ActivityMain.this, ActivityShowCarList.class);
                View sharedView = imgbtn_search_bg;
                String transitionName = getString(R.string.blue_name);

                ActivityOptions transitionActivityOptions = null;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    transitionActivityOptions = ActivityOptions.makeSceneTransitionAnimation(ActivityMain.this, sharedView, transitionName);
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    startActivity(FullScannerActivityIntent, transitionActivityOptions.toBundle());
                }

            }
        });

        tabHost.getTabWidget().getChildAt(0).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tabHost.setCurrentTab(0);
                tabHost.getTabWidget().getChildAt(1).setBackgroundColor(Color.parseColor("#002c3a"));
                tabHost.getTabWidget().getChildAt(2).setBackgroundColor(Color.parseColor("#002c3a"));
                tabHost.getTabWidget().getChildAt(3).setBackgroundColor(Color.parseColor("#002c3a"));
                Animator animator = null;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    int startradius = 0;
                    int endradius = Math.max(tabHost.getTabWidget().getChildAt(0).getWidth(), tabHost.getTabWidget().getChildAt(0).getHeight());
                    // finding X and Y co-ordinates
                    int cx = (tabHost.getTabWidget().getChildAt(0).getLeft() + tabHost.getTabWidget().getChildAt(0).getRight()) / 2;
                    int cy = (tabHost.getTabWidget().getChildAt(0).getTop() + tabHost.getTabWidget().getChildAt(0).getBottom()) / 2;

                    animator = ViewAnimationUtils.createCircularReveal(tabHost.getTabWidget().getChildAt(0), cx, cy, startradius, endradius);
                    animator.setInterpolator(new AccelerateDecelerateInterpolator());
                    animator.setDuration(350);

                    tabHost.getTabWidget().getChildAt(0).setBackgroundColor(Color.parseColor("#a61e22"));
                    animator.start();
                }
            }
        });

        tabHost.getTabWidget().getChildAt(1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tabHost.setCurrentTab(1);
                tabHost.getTabWidget().getChildAt(0).setBackgroundColor(Color.parseColor("#002c3a"));
                tabHost.getTabWidget().getChildAt(2).setBackgroundColor(Color.parseColor("#002c3a"));
                tabHost.getTabWidget().getChildAt(3).setBackgroundColor(Color.parseColor("#002c3a"));
                Animator animator1 = null;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    int startradius = 0;
                    int endradius = Math.max(tabHost.getTabWidget().getChildAt(1).getWidth(), tabHost.getTabWidget().getChildAt(1).getHeight());
                    // finding X and Y co-ordinates
                    int cx = (tabHost.getTabWidget().getChildAt(1).getLeft()) / 2;
                    int cy = (tabHost.getTabWidget().getChildAt(1).getTop() + tabHost.getTabWidget().getChildAt(1).getBottom()) / 2;

                    animator1 = ViewAnimationUtils.createCircularReveal(tabHost.getTabWidget().getChildAt(1), cx, cy, startradius, endradius);
                    animator1.setInterpolator(new AccelerateDecelerateInterpolator());
                    animator1.setDuration(350);

                    tabHost.getTabWidget().getChildAt(1).setBackgroundColor(Color.parseColor("#a61e22"));
                    animator1.start();
                }
            }
        });

        tabHost.getTabWidget().getChildAt(2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tabHost.setCurrentTab(2);
                tabHost.getTabWidget().getChildAt(0).setBackgroundColor(Color.parseColor("#002c3a"));
                tabHost.getTabWidget().getChildAt(1).setBackgroundColor(Color.parseColor("#002c3a"));
                tabHost.getTabWidget().getChildAt(3).setBackgroundColor(Color.parseColor("#002c3a"));
                Animator animator2 = null;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    int startradius = 0;
                    int endradius = Math.max(tabHost.getTabWidget().getChildAt(2).getWidth(), tabHost.getTabWidget().getChildAt(2).getHeight());
                    // finding X and Y co-ordinates
                    int cx = tabHost.getTabWidget().getChildAt(2).getWidth() / 2;
                    int cy = (tabHost.getTabWidget().getChildAt(2).getTop() + tabHost.getTabWidget().getChildAt(2).getBottom()) / 2;

                    animator2 = ViewAnimationUtils.createCircularReveal(tabHost.getTabWidget().getChildAt(2), cx, cy, startradius, endradius);
                    animator2.setInterpolator(new AccelerateDecelerateInterpolator());
                    animator2.setDuration(350);

                    tabHost.getTabWidget().getChildAt(2).setBackgroundColor(Color.parseColor("#a61e22"));
                    animator2.start();
                }
            }
        });
        tabHost.getTabWidget().getChildAt(3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tabHost.setCurrentTab(3);
                tabHost.getTabWidget().getChildAt(0).setBackgroundColor(Color.parseColor("#002c3a"));
                tabHost.getTabWidget().getChildAt(1).setBackgroundColor(Color.parseColor("#002c3a"));
                tabHost.getTabWidget().getChildAt(2).setBackgroundColor(Color.parseColor("#002c3a"));
                Animator animator3 = null;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    int startradius = 0;
                    int endradius = Math.max(tabHost.getTabWidget().getChildAt(3).getWidth(), tabHost.getTabWidget().getChildAt(3).getHeight());
                    // finding X and Y co-ordinates
                    int cx = tabHost.getTabWidget().getChildAt(3).getWidth() / 2;
                    int cy = (tabHost.getTabWidget().getChildAt(3).getTop() + tabHost.getTabWidget().getChildAt(3).getBottom()) / 2;

                    animator3 = ViewAnimationUtils.createCircularReveal(tabHost.getTabWidget().getChildAt(3), cx, cy, startradius, endradius);
                    animator3.setInterpolator(new AccelerateDecelerateInterpolator());
                    animator3.setDuration(350);

                    tabHost.getTabWidget().getChildAt(3).setBackgroundColor(Color.parseColor("#a61e22"));
                    animator3.start();
                }
            }
        });

        tabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
                imgbtn_search_bg.setVisibility(View.GONE);
                imgbtn_timer_bg.setVisibility(View.GONE);
            }
        });


        if (!sharedPreferencesHelper.getBooleanKey(sharedPreferencesHelper.Key_ShowHelp)) {
            Help();
            sharedPreferencesHelper.setBooleanKey(sharedPreferencesHelper.Key_ShowHelp, true);
        }


    }

    private void InitViews() {

        //get screen screenWidth and set pager screenWidth and height
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        G.width = displaymetrics.widthPixels;
        G.height = displaymetrics.heightPixels;

        tabHost = getTabHost();
        layout_tab = (RelativeLayout) findViewById(R.id.layout_tab);
        imgbtn_search_bg = (ImageButton) findViewById(R.id.imgbtn_search_bg);
        imgbtn_timer_bg = (ImageButton) findViewById(R.id.imgbtn_timer_bg);
    }

    private void setTabs() {

        Intent accountIntent = new Intent(this, ActivityAccount.class);
        Intent locationIntent = new Intent(this, ActivityLocation.class);
        Intent adsIntent = new Intent(this, ActivityShowAdsList.class);
        Intent travelIntent = new Intent(this, ActivityCreateTravel.class);

        addTab("location", R.drawable.ic_eye, locationIntent);
        addTab("travel", R.drawable.ic_add_travel, travelIntent);
        addTab("account", R.drawable.ic_person, accountIntent);
        addTab("ads", R.drawable.ic_ads, adsIntent);
    }

    private void addTab(String label_Id, int drawableId, Intent intent) {
        TabHost.TabSpec spec = tabHost.newTabSpec("tab" + label_Id);

        View tabIndicator = LayoutInflater.from(this).inflate(R.layout.tab_indicator, getTabWidget(), false);
        TextView title = (TextView) tabIndicator.findViewById(R.id.title);
        title.setText("");
        ImageView icon = (ImageView) tabIndicator.findViewById(R.id.icon);
        icon.setImageResource(drawableId);
        spec.setIndicator(tabIndicator);
        spec.setContent(intent);
        tabHost.addTab(spec);
    }

    private void Help() {
         /* setup enter and exit animation */
        final Animation enterAnimation = new AlphaAnimation(0f, 1f);
        enterAnimation.setDuration(600);
        enterAnimation.setFillAfter(true);

        final Animation exitAnimation = new AlphaAnimation(1f, 0f);
        exitAnimation.setDuration(600);
        exitAnimation.setFillAfter(true);

        mTourGuideHandler1 = TourGuide.init(this).with(TourGuide.Technique.Click)
                .setPointer(new Pointer())
                .setToolTip(new ToolTip().setTitle("لیست سفرها").setDescription("مبدا و مقصد خود را مشخص کنید و لیست سفرهای موجود را ببینید").setBackgroundColor(R.color.colorPrimaryLight)
                        .setGravity(Gravity.TOP))
                .setOverlay(new Overlay()
                        .setEnterAnimation(enterAnimation)
                        .setExitAnimation(exitAnimation)
                )
                .playOn(tabHost.getTabWidget().getChildAt(0));


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mTourGuideHandler1.cleanUp();
                tabHost.setCurrentTab(1);
                mTourGuideHandler2 = TourGuide.init(ActivityMain.this).with(TourGuide.Technique.Click)
                        .setPointer(new Pointer())
                        .setToolTip(new ToolTip().setTitle("افزودن سفر").setDescription("مبدا و مقصد خود را مشخص کنید و از دیگران دعوت کنید").setBackgroundColor(R.color.colorPrimaryLight)
                                .setGravity(Gravity.TOP))
                        .setOverlay(new Overlay()
                                .setEnterAnimation(enterAnimation)
                                .setExitAnimation(exitAnimation)
                        )
                        .playOn(tabHost.getTabWidget().getChildAt(1));
            }
        }, 4000);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mTourGuideHandler2.cleanUp();
                tabHost.setCurrentTab(2);
                mTourGuideHandler3 = TourGuide.init(ActivityMain.this).with(TourGuide.Technique.Click)
                        .setPointer(new Pointer())
                        .setToolTip(new ToolTip().setTitle("حساب کاربری").setDescription("اطلاعات حساب کاربری شما در این قسمت قرار دارد").setBackgroundColor(R.color.colorPrimaryLight)
                                .setGravity(Gravity.TOP))
                        .setOverlay(new Overlay()
                                .setEnterAnimation(enterAnimation)
                                .setExitAnimation(exitAnimation)
                        )
                        .playOn(tabHost.getTabWidget().getChildAt(2));
            }
        }, 8000);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mTourGuideHandler3.cleanUp();
                tabHost.setCurrentTab(3);
                mTourGuideHandler4 = TourGuide.init(ActivityMain.this).with(TourGuide.Technique.Click)
                        .setPointer(new Pointer())
                        .setToolTip(new ToolTip().setTitle("تخفیف ها").setDescription("تخفیف های ویژه شما در این قسمت قرار دارد").setBackgroundColor(R.color.colorPrimaryLight)
                                .setGravity(Gravity.TOP))
                        .setOverlay(new Overlay()
                                .setEnterAnimation(enterAnimation)
                                .setExitAnimation(exitAnimation)
                        )
                        .playOn(tabHost.getTabWidget().getChildAt(3));
            }
        }, 12000);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mTourGuideHandler4.cleanUp();
            }
        }, 16000);
    }

    boolean backPressed = false;
    @Override
    public void onBackPressed() {
        if(!backPressed){
            final Dialog dialog = new Dialog(ActivityMain.this);
            dialog.setContentView(R.layout.dialog_end_ad);
            dialog.show();

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    dialog.dismiss();
                }
            },2000);
            backPressed = true;
        }

    }
}

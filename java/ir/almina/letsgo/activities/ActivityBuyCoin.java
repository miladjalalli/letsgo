package ir.almina.letsgo.activities;

import android.animation.Animator;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;

import ir.almina.letsgo.R;
import ir.almina.letsgo.UiFont;

public class ActivityBuyCoin extends AppCompatActivity {

    Button buttonBuy;
    RadioButton radioButton1,radioButton2,radioButton3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_coin);
        UiFont.persianizer((ViewGroup) getWindow().getDecorView(), getApplicationContext());
        InitViews();
        setupToolbar();

        buttonBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(radioButton1.isChecked()){
                    Intent intent = new Intent(getBaseContext(), ActivityPayment.class);
                    intent.putExtra("EXTRA_COIN", "1");
                    intent.putExtra("EXTRA_COST", "2000");
                    startActivity(intent);
                }else if(radioButton2.isChecked()){
                    Intent intent = new Intent(getBaseContext(), ActivityPayment.class);
                    intent.putExtra("EXTRA_COIN", "2");
                    intent.putExtra("EXTRA_COST", "5000");
                    startActivity(intent);
                }else if(radioButton3.isChecked()){
                    Intent intent = new Intent(getBaseContext(), ActivityPayment.class);
                    intent.putExtra("EXTRA_COIN", "3");
                    intent.putExtra("EXTRA_COST", "10000");
                    startActivity(intent);
                }

            }
        });
        radioButton1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    radioButton2.setChecked(false);
                    radioButton3.setChecked(false);
                }
            }
        });
        radioButton2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    radioButton1.setChecked(false);
                    radioButton3.setChecked(false);
                }
            }
        });
        radioButton3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    radioButton1.setChecked(false);
                    radioButton2.setChecked(false);
                }
            }
        });
    }

    private void InitViews(){
        buttonBuy = (Button) findViewById(R.id.buttonBuy);
        radioButton1 = (RadioButton) findViewById(R.id.radioButton1);
        radioButton2 = (RadioButton) findViewById(R.id.radioButton2);
        radioButton3 = (RadioButton) findViewById(R.id.radioButton3);
    }

    public void setupToolbar() {
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(false);
        }
        toolbar.setBackgroundColor(Color.parseColor("#00ffffff"));

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Animator animator2 = null;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    int startradius = 0;
                    int endradius = Math.max(toolbar.getWidth(), toolbar.getHeight());
                    // finding X and Y co-ordinates
                    int cx = (toolbar.getLeft()) / 2;
                    int cy = (toolbar.getTop() + toolbar.getBottom()) / 2;

                    animator2 = ViewAnimationUtils.createCircularReveal(toolbar, cx, cy, startradius, endradius);
                    animator2.setInterpolator(new AccelerateDecelerateInterpolator());
                    animator2.setDuration(500);
                    toolbar.setBackgroundColor(Color.parseColor("#FF006C6D"));
                    findViewById(R.id.txtTitle).setVisibility(View.VISIBLE);
                    animator2.start();
                }
            }
        }, 200);
    }

}

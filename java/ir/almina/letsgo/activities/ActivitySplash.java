package ir.almina.letsgo.activities;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.eftimoff.androipathview.PathView;

import ir.almina.letsgo.R;
import ir.almina.letsgo.SharedPreferencesHelper;
import ir.almina.letsgo.UiFont;

public class ActivitySplash extends AppCompatActivity {
    SharedPreferencesHelper sharedPreferencesHelper;
    ImageView imgLogo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferencesHelper = new SharedPreferencesHelper(ActivitySplash.this);
        setContentView(R.layout.activity_splash);
        UiFont.persianizer((ViewGroup) getWindow().getDecorView(), getApplicationContext());
        InitViews();


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (sharedPreferencesHelper.getBooleanKey(sharedPreferencesHelper.Key_IsNotFirstRun) && sharedPreferencesHelper.getBooleanKey(sharedPreferencesHelper.Key_IsLogin)) {
                    startActivity(new Intent(ActivitySplash.this, ActivityMain.class));
                    ActivitySplash.this.finish();
                } else {
                    sharedPreferencesHelper.setBooleanKey(sharedPreferencesHelper.Key_IsNotFirstRun, true);
                    sharedPreferencesHelper.setBooleanKey(sharedPreferencesHelper.Key_IsLogin, false);
                    sharedPreferencesHelper.setStringKey(sharedPreferencesHelper.Key_Mobile, "");
                    sharedPreferencesHelper.setStringKey(sharedPreferencesHelper.Key_Name, "");
                    sharedPreferencesHelper.setStringKey(sharedPreferencesHelper.Key_Family, "");
                    sharedPreferencesHelper.setStringKey(sharedPreferencesHelper.Key_LisencePlate, "");
                    sharedPreferencesHelper.setStringKey(sharedPreferencesHelper.Key_NationalCode, "");
                    sharedPreferencesHelper.setBooleanKey(sharedPreferencesHelper.Key_ShowHelp, false);
                    sharedPreferencesHelper.setIntKey(sharedPreferencesHelper.Key_Coins, 0);
                    Intent Intent = new Intent(ActivitySplash.this, ActivitySighUp.class);
                    View sharedView = imgLogo;
                    String transitionName = getString(R.string.blue_name);

                    ActivityOptions transitionActivityOptions = null;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        transitionActivityOptions = ActivityOptions.makeSceneTransitionAnimation(ActivitySplash.this, sharedView, transitionName);
                    }
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        startActivity(Intent, transitionActivityOptions.toBundle());
                        ActivitySplash.this.finish();
                    }
                }
            }
        }, 2500);

    }

    private void InitViews() {
        imgLogo =  (ImageView) findViewById(R.id.imgLogo);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Animation rotateZoomAnimation = AnimationUtils.loadAnimation(ActivitySplash.this,R.anim.zoom_in_with_rotate_2);
                rotateZoomAnimation.setDuration(1500);
                imgLogo.startAnimation(rotateZoomAnimation);
            }
        },150);

    }
}

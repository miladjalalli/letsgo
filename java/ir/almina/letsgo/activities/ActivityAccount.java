package ir.almina.letsgo.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.renderscript.RSRuntimeException;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import ir.almina.letsgo.R;
import ir.almina.letsgo.SharedPreferencesHelper;
import ir.almina.letsgo.UiFont;
import ir.almina.letsgo.app.G;
import jp.wasabeef.picasso.transformations.internal.FastBlur;
import jp.wasabeef.picasso.transformations.internal.RSBlur;
import webservices.MinaSingleton;

import static webservices.WebServices.STR_URL_LOGIN;
import static webservices.WebServices.STR_URL_PROFILE_INFO;


public class ActivityAccount extends AppCompatActivity{

    ImageView imgBanner,imgBuyCoins;
    CircleImageView imgProfile;
    TextView txtMobile,txtName,txtFamily,txtNationalCode,txtLisencePlate,txtCoin;

    private int mRadius = 55;
    private int mSampling = 1;

    SharedPreferencesHelper sharedPreferencesHelper;


    @Override
    protected void onResume() {
        super.onResume();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                login(sharedPreferencesHelper.getStringKey(sharedPreferencesHelper.Key_Mobile));
            }
        },300);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferencesHelper = new SharedPreferencesHelper(ActivityAccount.this);
        setContentView(R.layout.activity_account);
        InitViews();
        UiFont.persianizer((ViewGroup) getWindow().getDecorView(), getApplicationContext());

        imgBuyCoins.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ActivityAccount.this, ActivityBuyCoin.class));

            }
        });

        txtMobile.setText(sharedPreferencesHelper.getStringKey(sharedPreferencesHelper.Key_Mobile));
        txtLisencePlate.setText(sharedPreferencesHelper.getStringKey(sharedPreferencesHelper.Key_LisencePlate));
        txtName.setText(sharedPreferencesHelper.getStringKey(sharedPreferencesHelper.Key_Name));
        txtFamily.setText(sharedPreferencesHelper.getStringKey(sharedPreferencesHelper.Key_Family));
        txtNationalCode.setText(sharedPreferencesHelper.getStringKey(sharedPreferencesHelper.Key_NationalCode));
        txtCoin.setText(sharedPreferencesHelper.getIntKey(sharedPreferencesHelper.Key_Coins)+"");

        findViewById(R.id.exit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedPreferencesHelper.remove(sharedPreferencesHelper.Key_IsNotFirstRun);
                sharedPreferencesHelper.remove(sharedPreferencesHelper.Key_IsLogin );
                sharedPreferencesHelper.remove(sharedPreferencesHelper.Key_ShowHelp);
                sharedPreferencesHelper.remove(sharedPreferencesHelper.Key_Coins );
                sharedPreferencesHelper.remove(sharedPreferencesHelper.Key_Mobile );
                sharedPreferencesHelper.remove(sharedPreferencesHelper.Key_Name  );
                sharedPreferencesHelper.remove(sharedPreferencesHelper.Key_Family );
                sharedPreferencesHelper.remove(sharedPreferencesHelper.Key_LisencePlate );

                Intent i = getBaseContext().getPackageManager()
                        .getLaunchIntentForPackage( getBaseContext().getPackageName() );
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });
    }



    private void InitViews(){
        imgBanner = (ImageView) findViewById(R.id.imgBanner);
        imgBuyCoins = (ImageView) findViewById(R.id.imgBuyCoins);
        imgProfile = (CircleImageView) findViewById(R.id.imgProfile);
        imgBanner.setLayoutParams(new LinearLayout.LayoutParams(G.width, (int) (G.width*0.75)));
        txtMobile = (TextView) findViewById(R.id.txtMobile);
        txtLisencePlate = (TextView) findViewById(R.id.txtLisencePlate);
        txtName = (TextView) findViewById(R.id.txtName);
        txtFamily = (TextView) findViewById(R.id.txtFamily);
        txtNationalCode = (TextView) findViewById(R.id.txtNationalCode);
        txtCoin = (TextView) findViewById(R.id.txtCoin);
    }


    public void login(final String strPhoneNumber) {
        Response.Listener<String> locResponse = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);

                    String code = jsonObject.getString("code");

                    if(code.equals("profile_failed")) {
                        String message = jsonObject.getString("message");
                    } else if(code.equals("profile_success")) {

                        String strFirstName = jsonObject.getString("first_name");
                        String strLastName = jsonObject.getString("last_name");
                        String strPhoneNumber = jsonObject.getString("phone_number");
                        String strAvatar = jsonObject.getString("avatar");
                        String strLicensePlate = jsonObject.getString("license_plate");
                        String strCoin = jsonObject.getString("coin");
                        String nationalCode = jsonObject.getString("national_code");

                        sharedPreferencesHelper.setIntKey(sharedPreferencesHelper.Key_Coins, Integer.parseInt(strCoin));




                        Log.i("Avatar",strAvatar);

                        txtMobile.setText(strPhoneNumber);
                        txtLisencePlate.setText(strLicensePlate);
                        txtName.setText(strFirstName);
                        txtFamily.setText(strLastName);
                        txtNationalCode.setText(nationalCode);
                        txtCoin.setText(strCoin);

                        Picasso.with(ActivityAccount.this).load(strAvatar)
                                .transform(new Transformation() {
                                    @Override
                                    public Bitmap transform(Bitmap source) {
                                        int scaledWidth = source.getWidth() / mSampling;
                                        int scaledHeight = source.getHeight() / mSampling;

                                        Bitmap bitmap = Bitmap.createBitmap(scaledWidth, scaledHeight, Bitmap.Config.ARGB_8888);

                                        Canvas canvas = new Canvas(bitmap);
                                        canvas.scale(1 / (float) mSampling, 1 / (float) mSampling);
                                        Paint paint = new Paint();
                                        paint.setFlags(Paint.FILTER_BITMAP_FLAG);
                                        canvas.drawBitmap(source, 0, 0, paint);

                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                                            try {
                                                bitmap = RSBlur.blur(getApplicationContext(), bitmap, mRadius);
                                            } catch (RSRuntimeException e) {
                                                bitmap = FastBlur.blur(bitmap, mRadius, true);
                                            }
                                        } else {
                                            bitmap = FastBlur.blur(bitmap, mRadius, true);
                                        }

                                        source.recycle();

                                        return bitmap;
                                    }

                                    @Override
                                    public String key() {
                                        return "BlurTransformation(radius=" + mRadius + ", sampling=" + mSampling + ")";
                                    }
                                }).centerInside().resize(G.width, (int) (G.width*0.75)).into(imgBanner);

                        Picasso.with(ActivityAccount.this).load(strAvatar).into(imgProfile);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener locErrorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.POST, STR_URL_PROFILE_INFO, locResponse, locErrorListener) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("phone_number", String.valueOf(strPhoneNumber));

                return params;
            }
        };

        MinaSingleton.getInstance(ActivityAccount.this).addToRequestQueue(stringRequest);
    }
    boolean backPressed = false;
    @Override
    public void onBackPressed() {
        if(!backPressed){
            final Dialog dialog = new Dialog(ActivityAccount.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_end_ad);
            dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            dialog.show();

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    dialog.dismiss();
                    finish();
                }
            },2500);
            backPressed = true;
        }

    }


}

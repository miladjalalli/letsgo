package ir.almina.letsgo.activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import ir.almina.letsgo.R;
import ir.almina.letsgo.UiFont;
import ir.almina.letsgo.app.G;

import static ir.almina.letsgo.R.id.map;

public class ActivityLocation extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private GoogleMap mMap;
    private Marker startMarker, endMarker;

    boolean startChooced;
    boolean endChooced;
    LinearLayout llData;
    Button btnSource,btnDestinition,btnConfirm;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
        UiFont.persianizer((ViewGroup) getWindow().getDecorView(), getApplicationContext());

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(map);
        initViews();

        mapFragment.getMapAsync(this);
        if (mapFragment == null) {
            customToast("متاسفیم،نقشه قابل بارگذاری نیست!");
        }

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ActivityLocation.this,ActivityShowCarList.class));
            }
        });

    }

    private void initViews() {
        llData = (LinearLayout) findViewById(R.id.llData);
        btnSource = (Button) findViewById(R.id.btnSource);
        btnDestinition = (Button) findViewById(R.id.btnDestinition);
        btnConfirm = (Button) findViewById(R.id.btnConfirm);
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMarkerClickListener(this);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(34.798200, 48.514880), 17.0f));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(17.0f));

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

        } else {
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setAllGesturesEnabled(true);
        }

        mMap.setTrafficEnabled(true);

        MarkerOptions startMarkerOption = new MarkerOptions().position(new LatLng(34.798200, 48.514880)).zIndex(1);
        startMarkerOption.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_flag_start));
        startMarker = mMap.addMarker(startMarkerOption);

        final MarkerOptions endMarkerOption = new MarkerOptions().position(new LatLng(34.798200, 48.514880)).zIndex(2);
        endMarkerOption.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_flag_end));
        endMarker = mMap.addMarker(endMarkerOption);

        endMarker.setVisible(false);

        mMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
            @Override
            public void onCameraMove() {
                if (!startChooced) {
                    startMarker.setPosition(googleMap.getCameraPosition().target);
                }
                if (!endChooced) {
                    endMarker.setPosition(googleMap.getCameraPosition().target);
                }
            }
        });

        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {

            }
        });

    }

    @Override
    public boolean onMarkerClick(Marker marker) {

        if (marker.equals(startMarker)) {
            try{
                startMarker.setTitle(getAddress(startMarker.getPosition()));
                btnSource.setText(getAddress(startMarker.getPosition()));
            }catch (Exception e){

            }
            startChooced = true;
            endMarker.showInfoWindow();
            endMarker.setPosition(getsomeMeterOver(startMarker.getPosition(), 50));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(getsomeMeterOver(startMarker.getPosition(), 50), 17.0f));
            customToast("برای تعیین مقصد صفحه را جا به جا کنید");
            endMarker.setVisible(true);
            endMarker.setZIndex(0.0f);
        }
        if (marker.equals(endMarker) && endChooced == false) {
            endChooced = true;
            customToast("لطفا چند لحظه صبر کنید");
            try{
                endMarker.setTitle(getAddress(endMarker.getPosition()));
                btnDestinition.setText(getAddress(endMarker.getPosition()));
            }catch (Exception e){

            }
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    llData.setVisibility(View.VISIBLE);
                    llData.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_in));
                }
            },0);

        }
        return false;
    }


    private void customToast(String text) {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.toast_layout_root));

        TextView textview = (TextView) layout.findViewById(R.id.text);
        textview.setText(text);

        Toast toast = new Toast(getApplicationContext());
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    private String getAddress(LatLng latLng) {
        Geocoder geocoder;
        List<Address> addresses = null;
        geocoder = new Geocoder(this, new Locale("fa"));

        try {
            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
        } catch (IOException e) {
            e.printStackTrace();
        }

//        String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
        String StreetName = addresses.get(0).getThoroughfare();
//        String city = addresses.get(0).getLocality();
//        String state = addresses.get(0).getAdminArea();
//        String country = addresses.get(0).getCountryName();
//        String postalCode = addresses.get(0).getPostalCode();
//        String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL

        return StreetName;
    }

    private LatLng getsomeMeterOver(LatLng latLng, int meter) {
        double meters = meter;
        double coef = meters * 0.0000089;
        double new_lat = latLng.latitude + coef;
        double new_long = latLng.longitude + coef / Math.cos(latLng.latitude * 0.018);
        return new LatLng(new_lat, new_long);
    }

    boolean backPressed = false;

    @Override
    public void onBackPressed() {
        if (startChooced && endChooced){
            llData.setVisibility(View.INVISIBLE);
            llData.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_out));
            endChooced = false;
            endMarker.setTitle("");
        }else if (startChooced && !endChooced){
            startChooced = false;
            startMarker.setTitle("");
            endMarker.setVisible(false);
        }else if (!startChooced && !endChooced){
            if(!backPressed){
                final Dialog dialog = new Dialog(ActivityLocation.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_end_ad);
                dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                dialog.show();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        dialog.dismiss();
                        finish();
                    }
                },2500);
                backPressed = true;
            }
        }
    }

}

package ir.almina.letsgo.activities;

import android.animation.Animator;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import ir.almina.letsgo.Models.Cars;
import ir.almina.letsgo.R;
import ir.almina.letsgo.UiFont;
import ir.almina.letsgo.adapters.carsAdapter;
import webservices.MinaSingleton;

import static webservices.WebServices.STR_URL_LOC;

public class ActivityShowCarList extends AppCompatActivity {
    ArrayList<Cars> carsArrayAdapter = new ArrayList<>();
    ListView listViewCars;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_car_list);
        UiFont.persianizer((ViewGroup) getWindow().getDecorView(), getApplicationContext());
        setupToolbar();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                progressDialog = new ProgressDialog(ActivityShowCarList.this);
                progressDialog.setTitle("در حال بارگذاری...");
                progressDialog.show();
            }
        }, 100);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getMatchUsers();
            }
        }, 100);

    }

    public void getMatchUsers(/*final double dblSourceLat, final double dblSourceLong, final String strSourceAddress,
                              final double dblDestinationLat, final double dblDestinationLong, final String strDestinationAddress,
                              final boolean blnOnlyOAE*/) {

        Response.Listener<String> locResponse = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String strFirstName = jsonObject.getString("first_name");
                        String strLastName = jsonObject.getString("last_name");
                        String strSource = jsonObject.getString("source");
                        String strDestination = jsonObject.getString("destination");
                        String strAvatar = jsonObject.getString("avatar_link");
                        String strTime = jsonObject.getString("time");
                        String strRate = jsonObject.getString("rate");

                        Cars cars = new Cars(strAvatar, strFirstName, strLastName, strSource, strDestination, strTime, strRate);
                        carsArrayAdapter.add(cars);
                    }
                    carsAdapter carsAdapter = new carsAdapter(ActivityShowCarList.this, carsArrayAdapter);
                    listViewCars = (ListView) findViewById(R.id.listViewCars);
                    listViewCars.setAdapter(carsAdapter);
                    progressDialog.dismiss();


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener locErrorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.POST, STR_URL_LOC, locResponse, locErrorListener) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                return params;
            }
        };

        MinaSingleton.getInstance(ActivityShowCarList.this).addToRequestQueue(stringRequest);
    }
    private void customToast(String text) {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.toast_layout_root));

        TextView textview = (TextView) layout.findViewById(R.id.text);
        textview.setText(text);

        Toast toast = new Toast(getApplicationContext());
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }


    public void setupToolbar() {
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(false);
        }
        toolbar.setBackgroundColor(Color.parseColor("#00ffffff"));

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Animator animator2 = null;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    int startradius = 0;
                    int endradius = Math.max(toolbar.getWidth(), toolbar.getHeight());
                    // finding X and Y co-ordinates
                    int cx = (toolbar.getLeft()) / 2;
                    int cy = (toolbar.getTop() + toolbar.getBottom()) / 2;

                    animator2 = ViewAnimationUtils.createCircularReveal(toolbar, cx, cy, startradius, endradius);
                    animator2.setInterpolator(new AccelerateDecelerateInterpolator());
                    animator2.setDuration(500);
                    toolbar.setBackgroundColor(Color.parseColor("#FF006C6D"));
                    findViewById(R.id.txtTitle).setVisibility(View.VISIBLE);
                    animator2.start();
                }
            }
        }, 200);
    }

}

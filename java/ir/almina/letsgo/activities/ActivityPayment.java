package ir.almina.letsgo.activities;

import android.app.ProgressDialog;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import ir.almina.letsgo.R;

public class ActivityPayment extends AppCompatActivity {

    String coin,cost;
    Button btnPay;
    TextView txtCoin,txtCost;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        InitViews();
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            coin = extras.getString("EXTRA_COIN");
            cost = extras.getString("EXTRA_COST");
            txtCoin.setText("تعداد سکه : "+coin+"");
            txtCost.setText("مبلغ : "+cost+"");
        }
        btnPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ProgressDialog progressDialog = new ProgressDialog(ActivityPayment.this);
                progressDialog.setTitle("لطفا صبر کنید");
                progressDialog.show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.dismiss();
                        customToast("پرداخت شد");
                        finish();
                    }
                },2000);
            }
        });
    }
    private void InitViews(){
        txtCoin = (TextView) findViewById(R.id.txtCoin);
        txtCost = (TextView) findViewById(R.id.txtCost);
        btnPay = (Button) findViewById(R.id.btnPay);
    }
    private void customToast(String text) {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.toast_layout_root));

        TextView textview = (TextView) layout.findViewById(R.id.text);
        textview.setText(text);

        Toast toast = new Toast(getApplicationContext());
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

}

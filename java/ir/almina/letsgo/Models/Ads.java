package ir.almina.letsgo.Models;

/**
 * Created by Milad on 12/13/2017.
 */

public class Ads {

    String name;
    String address;
    String tel;
    String logo;

    public Ads(String name, String address, String tel, String logo) {
        this.name = name;
        this.address = address;
        this.tel = tel;
        this.logo = logo;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }



    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getTel() {
        return tel;
    }

    public String getLogo() {
        return logo;
    }

}

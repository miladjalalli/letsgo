package ir.almina.letsgo.Models;

/**
 * Created by Milad on 12/13/2017.
 */

public class Cars {




    String avatar;
    String name;
    String family;
    String mabda;
    String maqsad;
    String time;
    String rating;

    public Cars(String avatar, String name, String family, String mabda, String maqsad, String time, String rating) {
        this.avatar = avatar;
        this.name = name;
        this.family = family;
        this.mabda = mabda;
        this.maqsad = maqsad;
        this.time = time;
        this.rating = rating;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public void setMabda(String mabda) {
        this.mabda = mabda;
    }

    public void setMaqsad(String maqsad) {
        this.maqsad = maqsad;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }


    public String getName() {
        return name;
    }

    public String getFamily() {
        return family;
    }

    public String getMabda() {
        return mabda;
    }

    public String getMaqsad() {
        return maqsad;
    }

    public String getTime() {
        return time;
    }

    public String getRating() {
        return rating;
    }
    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

}

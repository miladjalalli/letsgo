package ir.almina.letsgo.adapters;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import ir.almina.letsgo.Models.Ads;
import ir.almina.letsgo.Models.Cars;
import ir.almina.letsgo.R;

/**
 * Created by Milad on 12/13/2017.
 */

public class adsAdapter extends ArrayAdapter<Ads> {
    ArrayList<Ads> adsArrayAdapter = new ArrayList<>();
    Context context;
    private static LayoutInflater layoutInflater = null;

    public adsAdapter(@NonNull Context context, ArrayList<Ads> adsArrayAdapter) {
        super(context, R.layout.item_lv_ads);
        this.context = context;
        this.adsArrayAdapter= adsArrayAdapter;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        final View rowView = layoutInflater.inflate(R.layout.item_lv_ads, null);

        final ImageView imgLogo = (ImageView) rowView.findViewById(R.id.imgLogo);
        TextView txtName = (TextView) rowView.findViewById(R.id.txtName);
        TextView txtAddress = (TextView) rowView.findViewById(R.id.txtAddress);
        TextView txtTel = (TextView) rowView.findViewById(R.id.txtTel);

        Picasso.with(context).load(adsArrayAdapter.get(position).getLogo()).into(imgLogo);
        txtName.setText(adsArrayAdapter.get(position).getName());
        txtAddress.setText(adsArrayAdapter.get(position).getAddress());
        txtTel.setText(adsArrayAdapter.get(position).getTel());




        return rowView;
    }

    @Override
    public int getCount() {
        return adsArrayAdapter.size();
    }

    @Override
    public Ads getItem(int position) {
        return adsArrayAdapter.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

}

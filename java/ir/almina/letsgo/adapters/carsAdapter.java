package ir.almina.letsgo.adapters;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import ir.almina.letsgo.Models.Cars;
import ir.almina.letsgo.R;
import ir.almina.letsgo.SharedPreferencesHelper;
import ir.almina.letsgo.activities.ActivityCreateTravel;
import ir.almina.letsgo.activities.ActivityShowCarList;
import webservices.MinaSingleton;

import static com.google.android.gms.internal.zzagz.runOnUiThread;
import static webservices.WebServices.STR_URL_COIN_CHANGE;

/**
 * Created by Milad on 12/13/2017.
 */

public class carsAdapter extends ArrayAdapter<Cars> {
    ArrayList<Cars> carsArrayAdapter = new ArrayList<>();
    Context context;
    private static LayoutInflater layoutInflater = null;
    SharedPreferencesHelper sharedPreferencesHelper;

    public carsAdapter(@NonNull Context context, ArrayList<Cars> carsArrayAdapter) {
        super(context, R.layout.item_car_list);
        this.context = context;
        this.carsArrayAdapter = carsArrayAdapter;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        sharedPreferencesHelper = new SharedPreferencesHelper(context);
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        final View rowView = layoutInflater.inflate(R.layout.item_car_list, null);

        final CircleImageView imgAvatar = (CircleImageView) rowView.findViewById(R.id.imgAvatar);
        TextView txtName = (TextView) rowView.findViewById(R.id.txtName);
        TextView txtFamily = (TextView) rowView.findViewById(R.id.txtFamily);
        TextView txtMabda = (TextView) rowView.findViewById(R.id.txtMabda);
        TextView txtMaqsad = (TextView) rowView.findViewById(R.id.txtMaqsad);
        TextView txtTime = (TextView) rowView.findViewById(R.id.txtTime);
        RatingBar rating = (RatingBar) rowView.findViewById(R.id.rating);
        ImageButton btnReserve = (ImageButton) rowView.findViewById(R.id.btnReserve);

        Picasso.with(context).load(carsArrayAdapter.get(position).getAvatar()).into(imgAvatar);
        txtName.setText(carsArrayAdapter.get(position).getName());
        txtFamily.setText(carsArrayAdapter.get(position).getFamily());
        txtMabda.setText(carsArrayAdapter.get(position).getMabda());
        txtMaqsad.setText(carsArrayAdapter.get(position).getMaqsad());
        txtTime.setText(carsArrayAdapter.get(position).getTime());
        rating.setProgress(Integer.parseInt(carsArrayAdapter.get(position).getRating()));


        btnReserve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                coinChange(sharedPreferencesHelper.getStringKey(sharedPreferencesHelper.Key_Mobile), "-1",position);
            }
        });

        return rowView;
    }

    @Override
    public int getCount() {
        return carsArrayAdapter.size();
    }

    @Override
    public Cars getItem(int position) {
        return carsArrayAdapter.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void coinChange(final String strPhoneNumber, final String strValue, final int position) {
        Response.Listener<String> locResponse = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);

                    String code = jsonObject.getString("code");
                    final String message = jsonObject.getString("message");

                    if (code.equals("coin_failed")) {
                        Toast.makeText(context, "احراز حویت نا موفق بود : " + message, Toast.LENGTH_LONG).show();
                    } else if (code.equals("coin_less")) {
                        final ProgressDialog progressDialog = new ProgressDialog(context);
                        progressDialog.setTitle("لطفا صبر کنید");
                        progressDialog.show();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                progressDialog.dismiss();
                                Toast.makeText(context, "موجودی سکه ی شما کافی نیست ):", Toast.LENGTH_LONG).show();
                            }
                        },2000);
                    } else if (code.equals("coin_success")) {
                        final ProgressDialog progressDialog = new ProgressDialog(context);
                        progressDialog.setTitle("لطفا صبر کنید");
                        progressDialog.show();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                progressDialog.dismiss();
                                Dialog dialog = new Dialog(context);
                                dialog.setContentView(R.layout.dialog_car_info);
                                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                                final CircleImageView imgAvatar = (CircleImageView) dialog.findViewById(R.id.imgAvatar);
                                TextView txtCarModel = (TextView) dialog.findViewById(R.id.txtCarModel);
                                TextView txtMobile = (TextView) dialog.findViewById(R.id.txtMobile);
                                TextView txtName = (TextView) dialog.findViewById(R.id.txtName);
                                TextView txtFamily = (TextView) dialog.findViewById(R.id.txtFamily);
                                TextView txtMabda = (TextView) dialog.findViewById(R.id.txtMabda);
                                TextView txtMaqsad = (TextView) dialog.findViewById(R.id.txtMaqsad);
                                TextView txtTime = (TextView) dialog.findViewById(R.id.txtTime);
                                RatingBar rating = (RatingBar) dialog.findViewById(R.id.rating);



                                Picasso.with(context).load(carsArrayAdapter.get(position).getAvatar()).into(imgAvatar);
                                txtName.setText(carsArrayAdapter.get(position).getName());
                                txtFamily.setText(carsArrayAdapter.get(position).getFamily());
                                txtMabda.setText(carsArrayAdapter.get(position).getMabda());
                                txtMaqsad.setText(carsArrayAdapter.get(position).getMaqsad());
                                txtTime.setText(carsArrayAdapter.get(position).getTime());
                                txtCarModel.setText("پراید سفید");
                                txtMobile.setText("09369568742");
                                rating.setProgress(Integer.parseInt(carsArrayAdapter.get(position).getRating()));

                                dialog.show();
                            }
                        },2000);


                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener locErrorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.POST, STR_URL_COIN_CHANGE, locResponse, locErrorListener) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("phone_number", String.valueOf(strPhoneNumber));
                params.put("coin_value", String.valueOf(strValue));

                return params;
            }
        };

        MinaSingleton.getInstance(context.getApplicationContext()).addToRequestQueue(stringRequest);
    }


}

package ir.almina.letsgo;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


public class UiFont extends Activity{

    public static String TitleFont;
    public static String TranslateFont;

    public static void persianizer(ViewGroup viewgroup, Context context) {


        Typeface typeface = Typeface.createFromAsset(context.getAssets(),"fonts/iransans.ttf");

        int childCount = viewgroup.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View child = viewgroup.getChildAt(i);
            if (child instanceof ViewGroup) {
                persianizer((ViewGroup) child, context);
            }
            if (child instanceof TextView) {
                ((TextView) child).setTypeface(typeface);
            }
        }
    }

}

package ir.almina.letsgo;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Milad on 12/03/2017.
 */

public class SharedPreferencesHelper {

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;


    //KeyWord Names
    public String Key_IsNotFirstRun = "IsNotFirstRun";
    public String Key_IsLogin = "IsLogin";
    public String Key_ShowHelp = "ShowHelp";
    public String Key_Coins = "Coins";
    public String Key_Mobile = "Mobile";
    public String Key_Name = "Name";
    public String Key_Family = "Family";
    public String Key_LisencePlate = "LisencePlate";
    public String Key_NationalCode = "LisencePlate";

    public SharedPreferencesHelper(Context context) {
        sharedPreferences = context.getSharedPreferences("ir.almina.letsgo", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public void setStringKey(String key, String value) {
        editor.putString(key, value).apply();
    }

    public void setIntKey(String key, int value) {
        editor.putInt(key, value).apply();
    }

    public void setBooleanKey(String key, boolean value) {
        editor.putBoolean(key, value).apply();
    }

    public String getStringKey(String key) {
        return sharedPreferences.getString(key, null);
    }

    public int getIntKey(String key) {
        return sharedPreferences.getInt(key, 0);
    }

    public boolean getBooleanKey(String key) {
        return sharedPreferences.getBoolean(key, false);
    }

    public void remove(String key) {
        editor.remove(key).commit();
    }
}
